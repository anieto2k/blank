var gulp = require('gulp'),
    gutil = require('gulp-util'),
    jshint = require('gulp-jshint'),
    concat = require('gulp-concat'),
    rimraf = require('rimraf'),
    uglify = require('gulp-uglify'),
    minifyCSS = require('gulp-minify-css'),
    minifyHTML = require('gulp-minify-html'),
    autoprefixer = require('gulp-autoprefixer'),
    nodemon = require('gulp-nodemon'),
    zip = require('gulp-zip');


// Modules for webserver and livereload
var embedlr = require('gulp-embedlr'),
    refresh = require('gulp-livereload'),
    lrserver = require('tiny-lr')(),
    express = require('express'),
    livereload = require('connect-livereload');

/*************************************************************************************
	VARIABLES

*************************************************************************************/
var serverport = 3000;

var paths = {
  scripts: ['webapp/scripts/*.js', 'webapp/scripts/**/*.js'],
  maps: ['webapp/scripts/*.map', 'webapp/scripts/**/*.map'],
  styles: ['webapp/styles/*.css', 'webapp/styles/**/*.css'],
  fonts: ['webapp/fonts/*', 'webapp/fonts/**/*'],
  images: ['webapp/images/*', 'webapp/images/**/*'],
	htmls: ['webapp/views/*.html', 'webapp/views/**/*.html'],
	dist: {
		name: "public",
		path: "./public",
		css: "public/css",
    js: "public/js",
    img: "public/img",
    fonts: "public/fonts",
		views: "public/",
		watch: "public/**/*"
	}
}


/*************************************************************************************
  FONTS

*************************************************************************************/

// Styles task
gulp.task('fonts', function() {
  gulp.src(paths.fonts)
  .pipe(gulp.dest(paths.dist.fonts))
});

/*************************************************************************************
  IMAGES

*************************************************************************************/

// Styles task
gulp.task('images', function() {
  gulp.src(paths.images)
  .pipe(gulp.dest(paths.dist.img))
});

/*************************************************************************************
	STYLES

*************************************************************************************/

// Styles task
gulp.task('styles', function() {
  gulp.src(paths.styles)
  // Optionally add autoprefixer
  .pipe(autoprefixer("last 2 versions", "> 1%", "ie 8"))
  .pipe(concat('main.css'))
  // These last two should look familiar now :) 
  .pipe(minifyCSS({keepBreaks:true}))
  .pipe(gulp.dest(paths.dist.css))
});

/*************************************************************************************
	SCRIPTS
		- lint
		- compressJS

*************************************************************************************/

// Scripts task
gulp.task('scripts', ['compressJS']);



// JSHint task
gulp.task('lint', function() {
  gulp.src(paths.scripts)
  .pipe(jshint())
  .pipe(jshint.reporter('default'));
});

// Map
gulp.task('map', function() {
  // Single point of entry (make sure not to src ALL your files, browserify will figure it out)
  gulp.src(paths.maps)
  .pipe(gulp.dest(paths.dist.js));
});


// Javascript Compressor
gulp.task('compressJS', function() {
  // Single point of entry (make sure not to src ALL your files, browserify will figure it out)
  gulp.src(paths.scripts)
 // .pipe(uglify())
  .pipe(concat('main.js'))
  .pipe(gulp.dest(paths.dist.js));
});

/*************************************************************************************
	VIEWS

*************************************************************************************/
// Views task
gulp.task('views', function() {
	gulp.src('webapp/index.html')
    //.pipe(minifyHTML( {comments:true,spare:true}))	
		.pipe(gulp.dest('public/'))
    //.pipe(refresh(lrserver));

	gulp.src('webapp/views/**/*')
    //.pipe(minifyHTML( {comments:true,spare:true}))	
	  .pipe(gulp.dest('public/views/'))
    //.pipe(refresh(lrserver));

});


/*************************************************************************************
	DEV

*************************************************************************************/


// Dev task
gulp.task('dev', ['fonts', 'images', 'views', 'styles', 'scripts'], function() {
  	// Start webserver
     nodemon({ script: './bin/www', ext: 'html js' })
      .on('restart', function () {
        console.log('restarted!')
      })
  	// Run the watch task, to keep taps on changes
  	gulp.run('watch');
  	//.pipe(refresh(lrserver));
});

/*************************************************************************************
	CLEAN

*************************************************************************************/

gulp.task('clean', function (cb) {
    rimraf(paths.dist.path, cb);
});


/*************************************************************************************
	WATCH

*************************************************************************************/

// Watch task
gulp.task('watch', function() {
	// Ficheros Javascripts
  	gulp.watch(paths.scripts,[
  		'scripts'
  	]);

  	// Ficheros CSS
  	gulp.watch(paths.styles, [
    	'styles'
  	]);

  	// Ficheros HTML's
  	gulp.watch('webapp/**/*.html', [
        'views'
    ]);
});

/*************************************************************************************
	DEPLOY

*************************************************************************************/

// Deploy
gulp.task('deploy', ['views', 'styles', 'scripts']);

// Dist task
gulp.task('dist', ['deploy'], function(){
  return gulp.src(paths.dist.watch)
  			.pipe(zip('dist-' + (new Date().getTime()) + '.zip')) // 
			.pipe(gulp.dest("build"));
});
