var express = require('express')
    , path = require('path')
    , favicon = require('static-favicon')
    , logger = require('morgan')
    , cookieParser = require('cookie-parser')
    , bodyParser = require('body-parser')
    , fs = require('fs');


var app = express();

var entorno = process.env.ENTORNO || "LOCAL";
app.config = require("./config/config.json")[entorno];


// view engine setup
app.set('view engine', 'jade');
app.set('views', path.join(__dirname, 'app', 'views'));
app.use(express.static(__dirname + '/public'));

app.use(favicon());
app.use(logger('dev'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }) ); // to support URL-encoded bodies

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.log = require("./app/utils/utils").log(app.config.logLevel);

var dir = path.join(__dirname,'/app/controllers/');
fs.readdir(dir, function(err, files){
  if (err) throw err;
  files
    .filter(function(file) { return file.substr(-3) == '.js'; })
    .forEach(function(file){
      try {
        require(dir + file)(app);
        app.log.info("Controller: " + file + " cargado.");
      }catch(e){
        app.log.error("Error al cargar el controller: " + file);
        console.error(e);
      }     
  });
});

module.exports = app;
