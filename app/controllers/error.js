/**
 * ERROR CONTROLLER
 * Version: 0.0.1
 * Author: Andrés Nieto
 * 	
 * Se encarga de gestionar las acciones en caso de error de Express.
 */

module.exports = function(app){
	// development error handler
	// will print stacktrace
	if (app.get('env') === 'development') {
	    app.use(function(err, req, res, next) {
	        res.status(err.status || 500);
	        res.render('error', {
	            message: err.message,
	            error: err
	        });
	    });
	}

	// production error handler
	// no stacktraces leaked to user
	app.use(function(err, req, res, next) {
	    res.status(err.status || 500);
	    res.render('error', {
	        message: err.message,
	        error: {}
	    });
	});
}