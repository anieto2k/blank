/**
 * INDEX CONTROLLER
 * Version: 0.0.1
 * Author: Andrés Nieto
 * 	
 * Ejemplo de Index
 */



var path = "/";
module.exports = function(app){

	var message = "Bienvenido a Narnia!";
	
	app
	/**
	 * Versión HTML estatica
	 */
	.get(path, function(req, res, next){
		res.render("index", {title: message});
	})

	.get(path + "json", function(req, res, next){
		res.json({message: message});
	});
}