var util = require("util")
	, moment = require("moment")
	, colors = require('colors');




util.log = function(level){
	var levels = ['debug', 'info', 'warn', 'error', 'banner'];

	levels = levels.slice(levels.indexOf(level || 'info'));

	function write(type, message, color){
		if (typeof message != 'string') message = JSON.stringify(message);
		if (levels.indexOf(type) > -1){
			var txt = "[" + type.toUpperCase() + "] " + moment().format("DD/MM/YYYY HH:mm:ss ") + message;
			console.log(txt[color]);
		}
	}

	return {
		banner: function(message){
			write("banner", message, "magenta")
		},
		debug: function(message){
			write("debug", message, "grey")
		},
		info: function(message){
			write("info", message, "blue")
		},
		warn: function(message){
			write("warn", message, "yellow")
		},
		error: function(message){
			write("error", message, "red")
		}
	};
};


module.exports = util;